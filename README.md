Kleines Projekt, um das Rechnen mit Matrizen weniger rechenaufwändig zu machen.


Es wird externe Software benötigt, und zwar

* pdftoppm [Debian-Paket](https://packages.debian.org/stable/poppler-utils)
* LaTeX [Debian-Paket](https://packages.debian.org/stable/texlive), im Besonderen die LaTeX-Pakete inputenc, fontenc, babel, fouriernc


*Noch* wird das Programm hauptsächlich auf einem 1680x1050-Bildschirm entwickelt, und die Koordinaten der GUI-Bestandteile sind hartkodiert.

*Noch* funktionieren lediglich die elementaren Zeilenumformungen.


### Who do I talk to? ###

* Thure Dührsen (tdu@informatik.uni-kiel.de)