package supertest;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyButton extends JButton {

    private static final long serialVersionUID = 5377832195831728752L;
    private JFrame parentFrame = null;

    MyButton(JFrame parentFrame) {
        setParentFrame(parentFrame);
    }

    public JFrame getParentFrame() {
        return parentFrame;
    }

    public void setParentFrame(JFrame parentFrame) {
        this.parentFrame = parentFrame;
    }
}