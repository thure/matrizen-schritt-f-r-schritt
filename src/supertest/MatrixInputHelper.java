package supertest;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import static supertest.Stuff.*;

public class MatrixInputHelper extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 5149948375274063620L;

    private abstract class ButtonActionListener implements ActionListener {
	public JButton parent;

	public ButtonActionListener(JButton parentButton) {
	    this.parent = parentButton;
	}
    }

    Log log;
    
    JPanel panel;
    JLabel rowInputLabel;
    JLabel colInputLabel;
    JTextField rowInputField;
    JTextField colInputField;
    MyButton OKButton;
    MyButton cancelButton;

    public MatrixInputHelper(Log log) {

	this.log = log;
	
	this.getContentPane().setLayout(new BorderLayout(0, 3));
	this.frameInit();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	makeWindow();
	this.setVisible(true);

    }

    protected void makeWindow() {
	panel = new JPanel();
	panel.setLayout(null);
	rowInputLabel = new JLabel("Anzahl der Zeilen");
	rowInputField = new JTextField(1);
	colInputLabel = new JLabel("Anzahl der Spalten");
	colInputField = new JTextField(1);
	OKButton = new MyButton(this);
	OKButton.setText("OK");
	cancelButton = new MyButton(this);
	cancelButton.setText("Abbrechen");

	setPosition();
	addActionListeners();
	addComponents();
    }

    private void setPosition() {
	
	rowInputLabel.setBounds (  5, 5, 150, 25);
	rowInputField.setBounds (160, 5,  50, 25);
	
	colInputLabel.setBounds (  5, 30, 150, 25);	
	colInputField.setBounds (160, 30,  50, 25);
	
	OKButton.setBounds     (37,  70, 150, 25);
	cancelButton.setBounds (37, 100, 150, 25);
	
    }

    private void addActionListeners() {
	OKButton.addActionListener(new ButtonActionListener(OKButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		
		System.out.println("OK from Helper!");
		int rows = Integer.parseInt(rowInputField.getText());
		int cols = Integer.parseInt(colInputField.getText());
		System.out.println(rows + " rows");
		System.out.println(cols + " columns");		
		OKButton.getParentFrame().dispose();
		sleepFix();
		String title = "Eingabe der Matrix ("
		+ rows + " Zeilen, " + cols + " Spalten)";
		new MatrixInput(title, log, rows, cols).setBounds(200,
			200,
			500,500);
                // swapButton.getParentFrame().dispose(); 		
	    }
	});

	cancelButton.addActionListener(new ButtonActionListener(cancelButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		System.out.println("cancel"); System.exit(1);
	    }
	});
    }

    private void addComponents() {

	panel.add(rowInputLabel);
	panel.add(rowInputField);
	panel.add(colInputLabel);
	panel.add(colInputField);
	panel.add(OKButton);
	panel.add(cancelButton);
	this.add(panel);

    }

}
