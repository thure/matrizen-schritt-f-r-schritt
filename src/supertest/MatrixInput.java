package supertest;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import static supertest.Stuff.*;

public class MatrixInput extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = -6014555121727860109L;

    private abstract class ButtonActionListener implements ActionListener {
	public JButton parent;

	public ButtonActionListener(JButton parentButton) {
	    this.parent = parentButton;
	}
    }

    private Log log;
    private int rows = -1;
    private int cols = -1;
    private String[][] values = null;
    private String NewLine = System.getProperty("line.separator");

    JPanel infopanel;
    JPanel matrixInputPanel;
    JPanel buttonPanel;
    JTextArea infoArea;
    JTextArea matrixInputArea;
    MyButton OKButton;
    MyButton cancelButton;
    MyButton randomButton;
    MyButton moreButton;
    Random ran = new Random(); // good enough for non-crypto uses

    public MatrixInput(String title, Log log, int rows, int cols) {

	if (rows * cols <= 0) {
	    System.out.println("Nur positive Zahlen, bitte!");
	    System.exit(1);
	}

	if (rows > 10 || cols > 10) {
	    System.out.println("Maximal zehn Zeilen und Spalten!");
	    System.exit(1);
	}
	
	this.log = log;
	this.rows = rows;
	this.cols = cols;	
	this.getContentPane().setLayout(new BorderLayout(0, 3));
	this.frameInit();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	makeWindow();
	this.setTitle(title);
	this.setVisible(true);

    }

    protected void makeWindow() {
	infopanel = new JPanel();
	matrixInputPanel = new JPanel();
	buttonPanel = new JPanel();
	infopanel.setLayout(new FlowLayout());
	matrixInputPanel.setLayout(new FlowLayout());
	buttonPanel.setLayout(new FlowLayout());
	String infoText = "";
	infoText += "Matrixeinträge können ganze Zahlen " + NewLine;
	infoText += "oder Brüche sein." + NewLine;
	infoText += "Ganze Zahlen sind wie gewohnt einzugeben," + NewLine;
	infoText += "Brüche in  der Form   22/7   ." + NewLine;
	infoText += "Matrixeinträge sind durch Komma zu trennen," + NewLine;
	infoText += "Leerzeichen können nach Gusto verwendet werden," + NewLine;
	infoText += "Zeilenumbrüche ebenso." + NewLine;

	infoArea = new JTextArea();
	infoArea.setRows(rows);
	infoArea.setColumns(30);
	infoArea.setLineWrap(true);
	infoArea.setWrapStyleWord(true);
	infoArea.setEditable(false);
	infoArea.setText(infoText);

	matrixInputArea = new JTextArea();
	matrixInputArea.setRows(rows);
	matrixInputArea.setColumns(30);
	matrixInputArea.setLineWrap(true);
	matrixInputArea.setWrapStyleWord(true);

	makeInitialText();
	matrixInputArea.setText(makeInitialText());

	OKButton = new MyButton(this);
	OKButton.setText("OK");
	cancelButton = new MyButton(this);
	cancelButton.setText("Abbrechen");
	randomButton = new MyButton(this);
	randomButton.setText("Zufallszahlen");
	moreButton = new MyButton(this);
	moreButton.setText("Mehr ...");

	addActionListeners();
	addComponents();

    }

    private String makeInitialText() {
	String initial = "";
	for (int r = 1; r <= rows; r++) {
	    for (int c = 1; c <= cols; c++) {
		if (r == rows && c == cols) {
		    initial += "0";
		} else {
		    initial += "0, ";
		}
	    }
	    if (r < rows) initial += NewLine;
	}
	// System.err.println(initial);
	return initial;
    }
    
    private String makeRandomText() {
	String random = "";
	for (int r = 1; r <= rows; r++) {
	    for (int c = 1; c <= cols; c++) {
		
		int maxpos = 2*Math.max(rows, cols)-2;
		int offset = -maxpos/2+1;
		int modulus = 2*maxpos;
		
		int zahl = offset + ran.nextInt(modulus);
		String z = Integer.toString(zahl);
		
		if (r == rows && c == cols) {
		    random += z;
		} else {
		    random += z + ", ";
		}
	    }
	    if (r < rows) random += NewLine;
	}
	// System.err.println(initial);
	return random;
    }

    private void addActionListeners() {
	OKButton.addActionListener(new ButtonActionListener(OKButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("OK from the real thing!");

		OKButton.getParentFrame().dispose();
		sleepFix();

		String valueString = matrixInputArea.getText();
		
		valueString = Pattern.compile(" ")
			.matcher(valueString).replaceAll("");
		// Leerzeichen raus
		
		valueString = Pattern.compile(NewLine)
			.matcher(valueString).replaceAll("");
		// Zeilenumbrüche raus
		// komischerweise geht es mit
		// Pattern.compile(NewLine + " ")
		// nicht...
				
		valueString = Pattern.compile(",$").matcher(valueString)
			.replaceAll(""); // abschließendes Komma entfernen
		
		

		String[] values1d = valueString.split(",");		
		
		values = monoToBidi(values1d, rows, cols);		
		
		Matrix M = new Matrix("0", log, rows, cols, values);
		String[][] newValues = M.toLaTeX2PNGStringArray();		
		
		log.setBounds(200, 530, 500, 320);
		log.getOrigMatrixArea().append(M.toInputString() + NewLine);

		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Ausgangsmatrix",
			rows,
			cols,
			newValues,
			"42", // String id = "42" /// debug /// HACK
			true);
				
		new RowOpSelector(M, log).setBounds(1300, 530, 230, 420);
				

	    }
	});

	cancelButton.addActionListener(new ButtonActionListener(cancelButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		System.out.println("cancel");
		System.exit(1);
	    }
	});

	randomButton.addActionListener(new ButtonActionListener(randomButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		matrixInputArea.setText(makeRandomText());

	    }
	});
	
	moreButton.addActionListener(new ButtonActionListener(moreButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		
		moreButton.getParentFrame().dispose();

		System.out.println("Ich will noch mehr!");		
		
		values = new String[rows][cols];
		
		for (int i = 0; i < rows; i++) {
		    for (int j = 0; j < cols; j++) {
			values[i][j] = "0";
		    }		    
		}
				
		Matrix M = new Matrix("0",
		log, rows, cols, values);
		
		new SpecialMatrixSelector(M, log)
		.setBounds(1300, 530, 230, 420);

	    }
	});
    }

    private void addComponents() {

	infopanel.add(infoArea);
	matrixInputPanel.add(matrixInputArea);
	buttonPanel.add(OKButton);
	buttonPanel.add(cancelButton);
	buttonPanel.add(randomButton);
	buttonPanel.add(moreButton);
	this.add(infopanel, BorderLayout.NORTH);
	this.add(matrixInputPanel, BorderLayout.CENTER);
	this.add(buttonPanel, BorderLayout.SOUTH);

    }
}
