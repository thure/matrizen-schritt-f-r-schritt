package supertest;

import java.awt.FlowLayout;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.util.regex.Pattern;

public class Latex2png {

    public void make(String title,
	    int rows,
	    int cols,
	    String[][] values,
	    String prefix,
	    boolean debug) {
	
	// System.err.println("LaTeX: values");

	String TEMP_DIRECTORY = "/tmp/tdu/";
	String TEMP_TEX_FILE_NAME = "__TEX_"+prefix+"__";

	// 1. Prepare the .tex file
	String NewLine = System.getProperty("line.separator");
	String tex = "";
	tex += "\\documentclass[" + NewLine;
	tex += "border=0.50001bp," + NewLine;
	tex += "]{standalone}" + NewLine;

	tex += "\\usepackage[utf8]{inputenc}" + NewLine;
	tex += "\\usepackage[T1]{fontenc}" + NewLine;
	tex += "\\usepackage[ngerman]{babel}" + NewLine;
	// tex += "\\usepackage{amsfonts,amsmath}" + NewLine;
//	tex += "\\usepackage{fouriernc}" + NewLine;
//	tex += "% http://www.tug.dk/FontCatalogue/urwschoolbookl/" + NewLine;
	tex += "\\linespread{1.25}" + NewLine;
//	tex += "\\newcommand{\\field}[1]{\\mathbb{#1}}" + NewLine;
//	String[] numberSets = new String[] {"N", "Z", "Q", "R", "C"};
//	for (String s : numberSets) {            
//	    tex += "\\newcommand{\\"+s+"}{\\field{"+s+"}}" + NewLine;
//	}
//	tex += "\\newcommand{\\prim}{\\field{P}} % Primzahlen" + NewLine;
	tex += NewLine;
	tex += "\\begin{document}" + NewLine;
	tex += insertMatrix(rows, cols, values);
	tex += "\\end{document}";

	// 2. Create the .tex file
	FileWriter writer = null;
	try {
	    writer = new FileWriter(TEMP_DIRECTORY + "/" 
		    + TEMP_TEX_FILE_NAME + ".tex", false);
	    writer.write(tex, 0, tex.length());
	    writer.close();
	} catch (IOException ex) {
	    ex.printStackTrace();
	}

	// 3. Execute LaTeX from command line to generate picture
	ProcessBuilder pb = new ProcessBuilder("pdflatex", // "-shell-escape",
		TEMP_TEX_FILE_NAME + ".tex");
	// 3a. Convert pdf to png
	String resolution = new Integer(400).toString();
	ProcessBuilder pb2 = new ProcessBuilder("pdftoppm", "-r", resolution,
		"-png",
		TEMP_TEX_FILE_NAME + ".pdf", TEMP_TEX_FILE_NAME);

	pb.directory(new File(TEMP_DIRECTORY));
	try {
	    System.out.print("Running LaTeX...   ");
	    System.out.println(pb.command());
	    System.out.println();
	    Process p = pb.start();
	    StreamPrinter stdout = new StreamPrinter(p.getInputStream(), false);
	    StreamPrinter stderr = new StreamPrinter(p.getErrorStream(), false);
	    new Thread(stdout).start();
	    new Thread(stderr).start();
	    p.waitFor();
	} catch (IOException | InterruptedException ex) {
	    ex.printStackTrace();
	}

	pb2.directory(new File(TEMP_DIRECTORY));
	try {
	    System.out.print("Converting PDF to PNG...   ");
	    System.out.println(pb2.command());
	    System.out.println();
	    Process p = pb2.start();
	    StreamPrinter stdout = new StreamPrinter(p.getInputStream(), false);
	    StreamPrinter stderr = new StreamPrinter(p.getErrorStream(), false);
	    new Thread(stdout).start();
	    new Thread(stderr).start();
	    p.waitFor();
	} catch (IOException | InterruptedException ex) {
	    ex.printStackTrace();
	}

	String outfile = TEMP_DIRECTORY + TEMP_TEX_FILE_NAME + "-1.png";

	System.out.println(outfile);

	// 4. Display picture
	JFrame maFrame = new JFrame(title);
	maFrame.setResizable(false);
	maFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
	maFrame.getContentPane().setLayout(new FlowLayout());

	maFrame.getContentPane().add(new JLabel(new ImageIcon(outfile)));
	maFrame.pack();
	maFrame.setVisible(true);
	

	//5. Delete files

	if (! debug) {
	    for (File file : (new File(TEMP_DIRECTORY).listFiles())) {
		if (file.getName().startsWith(TEMP_TEX_FILE_NAME)) {
		    file.delete();
		}
		
	    } 
	}
    }

    private static String insertMatrix(int rows, int cols, String[][] values) {
	String NewLine = System.getProperty("line.separator");

	String m = "";

	m += "$" + NewLine;
	m += "\\left(" + NewLine;
	m += "  \\begin{array}{";

	if (rows * cols < 1) {
	    System.err.println("number of rows and/or columns is negative");
	    System.exit(1);
	}

	if (rows > 10 || cols > 10) {
	    System.err.println("more than 10 rows and/or columns");
	    System.exit(1);
	}

	for (int c = 1; c <= cols; c++) {
	    m += "r";
	}

	m += "}" + NewLine;

	for (int r = 0; r < rows; r++) {
	    for (int c = 0; c < cols; c++) {
		if (c == 0) m += "    ";
		 m += values[r][c];
		if (c < cols - 1) m += " & ";
	    }
	    
	    if (r < rows - 1) m += "\\\\";
	    	    
	    m += NewLine;
	}
	m += "  \\end{array}" + NewLine;
	m += "\\right)" + NewLine;
	m += "$" + NewLine;
	
	m = Pattern.compile("b\\{").matcher(m).replaceAll("\\\\frac\\{");
	// jetzt funktionieren LaTeX-Kontrollsequenzen, die auf "b" enden
	// und ein zwingend notwendiges Argument entgegennehmen, nicht mehr.
	// Fuer die Matrizen, um die es hier geht, duerfte das zu
	// verschmerzen sein.
		
	return m;
    }
}
