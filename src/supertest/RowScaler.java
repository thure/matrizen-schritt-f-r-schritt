package supertest;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import static supertest.Stuff.*;

public class RowScaler extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 2414949224902218227L;

    private abstract class ButtonActionListener implements ActionListener {
	public JButton parent;

	public ButtonActionListener(JButton parentButton) {
	    this.parent = parentButton;
	}
    }

    JPanel panel;
    
    JLabel rowLabel;
    JTextField rowinput;
    JLabel scaleLabel;
    JTextField scaleinput;
    MyButton okButton;    
    MyButton cancelButton;
    
    Matrix M;
    Log log;

    public Log getLog() {
        return log;
    }

    public void setLog(Log log) {
        this.log = log;
    }

    public RowScaler(Matrix M, Log log) {

	this.M = M;
	this.log = log;
	
	this.getContentPane().setLayout(new BorderLayout(0, 3));
	this.frameInit();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	makeWindow();
	this.setVisible(true);

    }

    protected void makeWindow() {
	panel = new JPanel();
	panel.setLayout(null);
	
	rowLabel = new JLabel("Skaliere Zeile Nr.");
	rowinput = new JTextField(1);
	scaleLabel = new JLabel("mit dem Faktor");
	scaleinput = new JTextField(2);
	
	okButton = new MyButton(this);
	okButton.setText("OK");
	
	cancelButton = new MyButton(this);
	cancelButton.setText("Abbrechen");

	setPosition();
	addActionListeners();
	addComponents();
    }

    private void setPosition() {
	
	rowLabel.setBounds(10, 10, 150, 25);
	rowinput.setBounds(140, 10, 25, 25);
	scaleLabel.setBounds(170, 10, 170, 25);
	scaleinput.setBounds(287, 10, 50, 25);
	
	okButton.setBounds     (60,  50, 200, 25);
	cancelButton.setBounds (60, 80, 200, 25);
	
    }

    private void addActionListeners() {
	okButton.addActionListener(new ButtonActionListener(okButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		
		System.out.println("OK!");
				
		okButton.getParentFrame().dispose();
		sleepFix();
		int i = Integer.parseInt(rowinput.getText());
		BigRational lambda = new BigRational(scaleinput.getText());
	
		Matrix result = M.scaleRow(i, lambda);
		
		result.setId(new Integer(Integer.parseInt(M.getId()) + 1)
			.toString());
		
		int rows = result.getNumberOfRows();
		int cols = result.getNumberOfCols();
		
		String[][] newValues = result.toLaTeX2PNGStringArray();
		
		result.print();
		
		GregorianCalendar c = new GregorianCalendar();
		
		// konstanter Dateiname // debug
		String id = "42";
		// String id = new Long(c.getTimeInMillis()).toString();
		
		// System.err.println(M.toLaTeX2PNGStringArray());
		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Umformungsschritt " + result.getId(),
			rows,
			cols, newValues, id, false);
	    }
	});

	cancelButton.addActionListener(new ButtonActionListener(cancelButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		System.out.println("keine weiteren!");
		cancelButton.getParentFrame().dispose();
		sleepFix();
	    }
	});
    }

    private void addComponents() {
	
	panel.add(rowLabel);
	panel.add(rowinput);
	panel.add(scaleLabel);
	panel.add(scaleinput);

	panel.add(okButton);
	panel.add(cancelButton);
	this.add(panel);

    }
}
