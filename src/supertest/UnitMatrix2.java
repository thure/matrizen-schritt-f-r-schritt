package supertest;

public class UnitMatrix2 extends Matrix {

    public UnitMatrix2(String id, Log log, int numberOfRows, int numberOfCols,
	    String[][] values) {
	super(id, log, numberOfRows, numberOfCols, values);
	
	if (numberOfRows != numberOfCols) {
	    throw new IllegalArgumentException("Matrix must be square");
	}
	
	for (int i = 0; i < numberOfRows; i++) {
	    for (int j = 0; j < numberOfCols; j++) {
		values[i][j] = (i==j ? "0" : "1");
	    }	    
	}	
    }
    
    public UnitMatrix2(String id, Log log, int numberOfRows, int numberOfCols,
	    BigRational[][] values) {
	super(id, log, numberOfRows, numberOfCols, values);
	
	if (numberOfRows != numberOfCols) {
	    throw new IllegalArgumentException("Matrix must be square");
	}
	
	for (int i = 0; i < numberOfRows; i++) {
	    for (int j = 0; j < numberOfCols; j++) {
		values[i][j] = (i==j ? BigRational.ZERO : BigRational.ONE);
	    }	    
	}	
    }    
}
