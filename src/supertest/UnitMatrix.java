package supertest;

public class UnitMatrix extends Matrix {

    public UnitMatrix(String id, Log log, int numberOfRows, int numberOfCols,
	    String[][] values) {
	super(id, log, numberOfRows, numberOfCols, values);
	
	if (numberOfRows != numberOfCols) {
	    throw new IllegalArgumentException("Matrix must be square");
	}
	
	for (int i = 0; i < numberOfRows; i++) {
	    for (int j = 0; j < numberOfCols; j++) {
		values[i][j] = (i==j ? "1" : "0");
	    }	    
	}	
    }
    
    public UnitMatrix(String id, Log log, int numberOfRows, int numberOfCols,
	    BigRational[][] values) {
	super(id, log, numberOfRows, numberOfCols, values);
	
	if (numberOfRows != numberOfCols) {
	    throw new IllegalArgumentException("Matrix must be square");
	}
	
	for (int i = 0; i < numberOfRows; i++) {
	    for (int j = 0; j < numberOfCols; j++) {
		values[i][j] = (i==j ? BigRational.ONE : BigRational.ZERO);
	    }	    
	}	
    }    
}
