package supertest;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import static supertest.Stuff.*;

public class RowSwapper extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 6459130277488760407L;

    private abstract class ButtonActionListener implements ActionListener {
	public JButton parent;

	public ButtonActionListener(JButton parentButton) {
	    this.parent = parentButton;
	}
    }

    JPanel panel;
    
    
    JLabel row1Label;
    JTextField row1input;
    JLabel row2Label;
    JTextField row2input;
    MyButton okButton;    
    MyButton cancelButton;
    
    Matrix M;
    Log log;

    public Log getLog() {
        return log;
    }

    public RowSwapper(Matrix M, Log log) {
	
	this.M = M;
	this.log = log;

	this.getContentPane().setLayout(new BorderLayout(0, 3));
	this.frameInit();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	makeWindow();
	this.setVisible(true);

    }

    protected void makeWindow() {
	panel = new JPanel();
	panel.setLayout(null);
	
	row1Label = new JLabel("Vertausche Zeile Nr.");
	row1input = new JTextField(1);
	row2Label = new JLabel("mit Zeile Nr.");
	row2input = new JTextField(1);
	
	okButton = new MyButton(this);
	okButton.setText("OK");
	
	cancelButton = new MyButton(this);
	cancelButton.setText("Abbrechen");

	setPosition();
	addActionListeners();
	addComponents();
    }

    private void setPosition() {
	
	row1Label.setBounds(10, 10, 180, 25);
	row1input.setBounds(170, 10, 25, 25);
	row2Label.setBounds(200, 10, 150, 25);
	row2input.setBounds(300, 10, 25, 25);
	
	okButton.setBounds     (70,  50, 200, 25);
	cancelButton.setBounds (70, 80, 200, 25);
	
    }

    private void addActionListeners() {
	okButton.addActionListener(new ButtonActionListener(okButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		
		System.out.println("OK!");
				
		okButton.getParentFrame().dispose();
		sleepFix();
		
		int row1index = Integer.parseInt(row1input.getText());
		int row2index = Integer.parseInt(row2input.getText());
		
		Matrix result = M.swapRows(row1index, row2index);
		
		result.setId(new Integer(Integer.parseInt(M.getId()) + 1)
			.toString());
		
		int rows = result.getNumberOfRows();
		int cols = result.getNumberOfCols();
		
		String[][] newValues = result.toLaTeX2PNGStringArray();
		
		result.print();
		
		GregorianCalendar c = new GregorianCalendar();
		
		// konstanter Dateiname // debug
		String id = "42";
		// String id = new Long(c.getTimeInMillis()).toString();
		
		// System.err.println(M.toLaTeX2PNGStringArray());
		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Umformungsschritt " + result.getId(),
			rows,
			cols, newValues, id, false);
	    }
	});

	cancelButton.addActionListener(new ButtonActionListener(cancelButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		System.out.println("keine weiteren!");
		cancelButton.getParentFrame().dispose();
		sleepFix();
	    }
	});
    }

    private void addComponents() {
	
	panel.add(row1Label);
	panel.add(row1input);
	panel.add(row2Label);
	panel.add(row2input);

	panel.add(okButton);
	panel.add(cancelButton);
	this.add(panel);

    }

}
