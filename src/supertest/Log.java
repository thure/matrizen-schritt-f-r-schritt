package supertest;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import static supertest.Stuff.*;

public class Log extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 2405029597497223345L;

    private abstract class ButtonActionListener implements ActionListener {
	public JButton parent;

	public ButtonActionListener(JButton parentButton) {
	    this.parent = parentButton;
	}
    }
    
    JPanel origMatrixpanel;
    JPanel protocolPanel;
    JPanel buttonPanel;
    JScrollPane protocolScroller;
    JScrollPane origMatrixScroller;
    JTextArea origMatrixArea;
    JTextArea protocolArea;
    MyButton closeButton;

    public Log(String title) {

	this.getContentPane().setLayout(new BorderLayout(0, 3));
	this.frameInit();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	makeWindow();
	this.setTitle(title);
	this.setVisible(true);

    }

    public JTextArea getOrigMatrixArea() {
        return origMatrixArea;
    }

    protected void makeWindow() {
	origMatrixpanel = new JPanel();
	protocolPanel = new JPanel();
	buttonPanel = new JPanel();
	origMatrixpanel.setLayout(new FlowLayout());
	protocolPanel.setLayout(new FlowLayout());
	buttonPanel.setLayout(new FlowLayout());
	origMatrixArea = new JTextArea();
	
	origMatrixArea.setRows(8);
	origMatrixArea.setColumns(30);
	origMatrixArea.setLineWrap(true);
	origMatrixArea.setWrapStyleWord(true);
	origMatrixArea.setEditable(false);
	
	String infoText = "";
	infoText += "Ausgangsmatrix:" + NewLine() + NewLine();
	origMatrixArea.setText(infoText);

	protocolArea = new JTextArea();
	protocolArea.setRows(8);
	protocolArea.setColumns(30);
	protocolArea.setLineWrap(true);
	protocolArea.setWrapStyleWord(true);
	origMatrixArea.setEditable(false);
	
	origMatrixScroller = new JScrollPane(origMatrixArea);
	origMatrixScroller.setPreferredSize(new Dimension(350, 120));
	protocolScroller = new JScrollPane(protocolArea);
	protocolScroller.setPreferredSize(new Dimension(350, 120));

	protocolArea.setText("");

	closeButton = new MyButton(this);
	closeButton.setText("Schließen");
	
	addActionListeners();
	addComponents();

    }
    
    private void addActionListeners() {

	closeButton.addActionListener(new ButtonActionListener(closeButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		System.out.println("close");
		closeButton.getParentFrame().dispose();
	    }
	});
    }

    private void addComponents() {

	// origMatrixpanel.add(origMatrixArea);
	origMatrixpanel.add(origMatrixScroller, BorderLayout.CENTER);
	protocolPanel.add(protocolScroller, BorderLayout.CENTER);
	buttonPanel.add(closeButton);
	this.add(origMatrixpanel, BorderLayout.NORTH);
	this.add(protocolPanel, BorderLayout.CENTER);
	this.add(buttonPanel, BorderLayout.SOUTH);

    }
    
    public void append (String what) {
	protocolArea.append(what);
    }
}
