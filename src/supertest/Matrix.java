package supertest;

import java.math.BigInteger;

import static supertest.Stuff.*;

public class Matrix {

    private int numberOfRows = -1;
    private int numberOfCols = -1;
    protected BigRational[][] values;
    private String id;
    private Log log;
    private String line = "----------------------------------------"
                           + NewLine();

    public String getId() {
	return id;
    }

    public void setId(String id) {
	this.id = id;
    }

    public int getNumberOfRows() {
	return numberOfRows;
    }

    public int getNumberOfCols() {
	return numberOfCols;
    }

    public BigRational[][] getValues() {
	return values;
    }
    
    public BigRational v(int r, int c) {
	return values[r-1][c-1];
    }

    public void setNumberOfRows(int numberOfRows) {
	this.numberOfRows = numberOfRows;
    }

    public void setNumberOfCols(int numberOfCols) {
	this.numberOfCols = numberOfCols;
    }

    public void setValues(BigRational[][] values) {
	this.values = values;
    }

    public Matrix(String id, Log log, int numberOfRows, int numberOfCols,
	    BigRational[][] values) {

	this.id = id;
	this.numberOfRows = numberOfRows;
	this.numberOfCols = numberOfCols;
	this.values = values;
	this.log = log;
    }

    public Matrix(String id, Log log, int numberOfRows, int numberOfCols,
	    String[][] valuesAsString) {

	this.id = id;
	this.numberOfRows = numberOfRows;
	this.numberOfCols = numberOfCols;
	this.values = new BigRational[numberOfRows][numberOfCols];

	for (int i = 1; i <= numberOfRows; i++) {
	    for (int j = 1; j <= numberOfCols; j++) {

		this.values[i - 1][j - 1] = new BigRational(
			valuesAsString[i - 1][j - 1]);
	    }
	}
	this.log = log;
    }

    public void setRow(int i, BigRational[] newValues) {
	if (this.getNumberOfCols() != newValues.length) {
	    System.err.println("Invalid dimensions for updating row");
	    System.exit(1);
	}

	System.arraycopy(newValues, 0, values, (i - 1) * numberOfCols,
		numberOfCols);
    }

    public BigRational[] getRow(int i) {
	if (i < 1 || i > numberOfRows) {
	    System.err.println("Invalid row number" + i + " for getRow");
	}
	BigRational[] result = new BigRational[numberOfCols];
	
	result = values[i-1];
	//	System.arraycopy(values, (i-1) * numberOfCols, result, 0,       numberOfCols);
//	System.arraycopy(src,    srcPos,               dest,   destPos, length);
	return result;
    }
    
    public BigInteger gcdRowDenominators (int r) {
	
	// ggT der Nenner der Brüche in einer Zeile
	
	BigRational[] row = getRow(r);
	BigInteger[] denoms = new BigInteger[numberOfCols];
	
	for (int c = 0; c < numberOfCols; c++) {
	    denoms[c] = row[c].getDenominator();
	}	
	    return gcdArray(denoms);
    }
    
    public BigInteger gcdRowEntries (int r) {
	
	// ggT der ganzen Zahlen in einer Zeile
	
	BigRational[] row = getRow(r);
	BigInteger[] integersInRow = new BigInteger[numberOfCols];
	
	for (int c = 0; c < numberOfCols; c++) {
	    integersInRow[c] = row[c].getNumerator();
	}	
	    return gcdArray(integersInRow);
    }
    
    public BigInteger lcmRowDenominators (int r) {
	
	BigRational[] row = getRow(r);
	BigInteger[] denoms = new BigInteger[numberOfCols];
	
	for (int c = 0; c < numberOfCols; c++) {
	    denoms[c] = row[c].getDenominator();
	}	
	    return lcmArray(denoms);
    }

    public static void swapNumbers(BigRational x, BigRational y) {
	// Example: initially x = 10, y = 5
	// x = x + y; // x now becomes 15
	// y = x - y; // y becomes 10
	// x = x - y; // x becomes 5

	x.plus(y);
	y.negate().plus(x);
	x.minus(y);
    }

    public Matrix addRows(int from, int to) {

	if (from * to <= 0 || from * to > numberOfRows * numberOfCols) {
	    System.err.println("Invalid row numbers for adding rows");
	    System.exit(1);
	}

	from = from - 1;
	to = to - 1;

	for (int c = 0; c < numberOfCols; c++) {
	    values[to][c] = values[to][c].plus(values[from][c]);
	}
	
	String what = "Addiere Zeile Nr. " + (from + 1)
		+ " zur Zeile Nr. " + (to + 1) + NewLine();
	log.append(what);System.out.println(what);
	this.print();
	return this;
    }

    public Matrix swapRows(int i, int j) {
	// probably ok

	if (i == j) {
	    this.print();
	    return this;
	}
	    

	if (i * j <= 0 || i * j > numberOfRows * numberOfCols) {
	    System.err.println("Invalid row numbers for swapping rows");
	    System.exit(1);
	}

	i = i - 1;
	j = j - 1;

	for (int c = 0; c < numberOfCols; c++) {

	    BigRational temp;

	    temp = values[i][c];
	    values[i][c] = values[j][c];
	    values[j][c] = temp;
	}
	String what = "Vertausche Zeile Nr. " + (i+1)
		+ " mit Zeile Nr. " + (j+1) + NewLine();
	log.append(what); System.out.println(what);
	this.print();
	return this;
    }

    public Matrix scaleRow(int i, BigRational lambda) {

	if (i < 1 || i > numberOfRows) {
	    System.err.println("scaleRow: " + i + ": no such row");
	    System.exit(1);
	}

	if (lambda.compareTo(BigRational.ONE) == 0) {
	    this.print();
	    return this;
	}

	i = i - 1;

	for (int c = 0; c < numberOfCols; c++) {
	    values[i][c] = values[i][c].times(lambda);
	}

	String what = "Skaliere Zeile Nr. " + (i+1)
		+ " mit dem Faktor " + lambda + NewLine();
	log.append(what);System.out.println(what);
	this.print();
	return this;
    }
    
    private int getFirstRowIndexWithFractions() {

	int rows = this.getNumberOfRows();
	int cols = this.getNumberOfCols();

	for (int r = 1; r <= rows; r++) {
	    BigRational[] row = this.getRow(r);
	    for (int c = 0; c < cols; c++) {
		BigRational value = row[c];
		if (!value.isIntegral()) {
		    return r;
		}
	    }
	}
	return -1;
    }

    private Matrix killFractionsInRow(int r) {

	BigInteger lcm = this.lcmRowDenominators(r);
	System.out.println("Brüche beseitigen in Zeile "+ r +": kgV der Nenner ist" + lcm);
	BigRational rlcm = new BigRational(lcm, BigInteger.ONE);
	
	return this.scaleRow(r, rlcm);
    }
    
    public Matrix killAllFractions () {
	
	log.append(line);
	log.append("Brüche in allen Zeilen beseitigen:" + NewLine());
	
	int rowToClean = -2;
	
	while ((rowToClean = getFirstRowIndexWithFractions()) != -1) {
	    killFractionsInRow(rowToClean);
	}
	log.append(line);
	return this;
    }
    
    private Matrix pullOutGCDInRow(int r) {
	Matrix result = this;
	BigInteger gcd = this.gcdRowEntries(r);
	if (gcd.compareTo(BigInteger.ZERO) != 0) {
	    System.err.println(
		    "ggT in der " + r + "-ten Zeile ist gleich " + gcd);
	    BigRational oneOverGCD = new BigRational(BigInteger.ONE, gcd);
	    result = this.scaleRow(r, oneOverGCD);
	}
	return result;
    }
 
    public Matrix pullOutGCDinAllRows() {
	
	
	log.append(line);
	log.append("ggT aus allen Zeilen herausziehen:" + NewLine());
	
	
	for (int i = 1; i <= numberOfRows; i++) {
	    
	    pullOutGCDInRow(i);
	}
	log.append(line);
	return this;
    }
    
    private int findNonZeroEntryInColumn (final int c) {
	if (c < 1 || c > numberOfCols) {
	    System.err.println("invalid row in findNonZeroEntryInColumn");
	}
	
	int startingRow = c+1;
	
	for (int row = startingRow; row <= numberOfRows; row++) {
	    if (row == c) continue; // nicht das Diagonalelement prüfen
	    if (v(row,c).isNonZero()) return row;
	}
	
	return -1; // kein Nichtnullelement gefunden	
    }
    
    public Matrix GaussianElimination() {
	putZeroRowsToBottom();
	log.append(line);
        String desc = "Zeilenstufenform";
	log.append(desc + NewLine());	
	int maxcol = Math.min(numberOfRows, numberOfCols);
	for (int c = 1; c <= maxcol; c++) {
	    System.out.println("Bearbeite Spalte " + c);
	    int startingRow = c+1;
	    if (v(c, c).isZero()) {

		int nzi = findNonZeroEntryInColumn(c);
		if (nzi == -1) continue; // mit der nächsten Spalte fortfahren
		// vermutlich FALSCH!!!
		
		this.swapRows(c, nzi);
	    }
	    // Jetzt ist das Diagonalelement in der aktuellen Spalte
	    // ungleich Null. Wir machen es positiv.	    
	    if (v(c,c).isNegative()) {
		this.scaleRow(c, BigRational.ONE.negate());
	    }
	    
	    for (int row = startingRow; row <= numberOfRows; row++) {
		if (row == c) continue; // nicht das Diagonalelement prüfen
		BigRational lambda = v(row,c).divideBy(v(c,c)).negate();
		if (lambda.isZero()) continue; // da steht schon eine Null		
		scaleRow(c, lambda);
		addRows(c, row);
		scaleRow(c, lambda.reciprocal());
		
	    }
	}	
	log.append(line);
	return this;
    }
    
    private void putZeroRowsToBottom() {
	
    }

    public Matrix toRowEchelonForm () {
	return GaussianElimination();
    }
    
    public Matrix toReducedRowEchelonForm () {
	Matrix r = GaussianElimination();
	// jetzt reduzieren
	return r;
    }
    
    

    public void print() {
	for (int i = 1; i <= numberOfRows; i++) {
	    for (int j = 1; j <= numberOfCols; j++) {
		BigRational value = values[i - 1][j - 1];
		System.out.print(value + "   ");
	    }
	    System.out.println();
	}
    }

    public String toInputString() {
	String result = "";
	for (int r = 1; r <= numberOfRows; r++) {
	    for (int c = 1; c <= numberOfCols; c++) {

		BigRational value = values[r - 1][c - 1];
		String z = value.toString();

		if (r == numberOfRows && c == numberOfCols) {
		    result += z;
		} else {
		    result += z + ", ";
		}
	    }
	    if (r < numberOfRows)
		result += NewLine();
	}
	return result;
    }

    public String[][] toLaTeX2PNGStringArray() {
	String[][] result = new String[numberOfRows][numberOfCols];
	for (int r = 1; r <= numberOfRows; r++) {
	    for (int c = 1; c <= numberOfCols; c++) {

		BigRational value = values[r - 1][c - 1];
		String z = value.tex();

		result[r - 1][c - 1] = z;
	    }
	}
	return result;
    }

    public Log getLog() {
	// TODO Auto-generated method stub
	return this.log;
    }    
}
