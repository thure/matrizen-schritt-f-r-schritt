package supertest;

import java.math.BigInteger;

public class Stuff {
    
    private static String NewLine = System.getProperty("line.separator");

    public static String NewLine() {
        return NewLine;
    }

    public static String[][] monoToBidi(final Object[] array, final int rows,
	    final int cols) {
	// https://stackoverflow.com/a/14605976
	// 2016-09-21
	if (array.length != (rows * cols))
	    throw new IllegalArgumentException("Invalid array length");

	String[][] bidi = new String[rows][cols];
	for (int i = 0; i < rows; i++)
	    System.arraycopy(array, (i * cols), bidi[i], 0, cols);

	return bidi;
    }

    public static void sleepFix() {
	try {
	    Thread.sleep(350);
	} catch (InterruptedException e) {
	}
    }

    public static void sleep(long ms) {
	try {
	    Thread.sleep(ms);
	} catch (InterruptedException e) {
	}
    }
    
    public static BigInteger gcdArray (BigInteger[] numbers) {
	int l = numbers.length;
	if (l < 1) {
	    System.err.println("gcd of no numbers...");
	}
	BigInteger result = numbers[0];
	for(int i = 1; i < l; i++) {
	    result = result.gcd(numbers[i]);
	}
	return result;
    }
    
    public static BigInteger lcm (BigInteger a, BigInteger b) {
	BigInteger gcd = a.gcd(b);
	return a.divide(gcd).multiply(b);
    }
    
    public static BigInteger lcmArray (BigInteger[] numbers) {
	int l = numbers.length;
	if (l < 1) {
	    System.err.println("lcm of no numbers...");
	}
	BigInteger result = numbers[0];
	for(int i = 1; i < l; i++) {
	    result = lcm(result, numbers[i]);
	}
	return result;
    }

}
