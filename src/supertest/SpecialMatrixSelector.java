package supertest;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
// import java.util.GregorianCalendar;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import static supertest.Stuff.*;

public class SpecialMatrixSelector extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = -5712080261092922467L;

    private abstract class ButtonActionListener implements ActionListener {
	public MyButton parent;

	public ButtonActionListener(MyButton parentButton) {
	    this.parent = parentButton;
	}
    }

    JPanel panel;

    JLabel infoLabel;
    MyButton einheitsMatrixButton;
    MyButton einheitsmatrix2Button;
    MyButton diagonalmatrixButton;
    MyButton hilbertmatrixButton;
    MyButton begleitmatrixButton;    
    MyButton keineButton;

    Matrix M;
    Log log;

    public SpecialMatrixSelector(Matrix M, Log log) {

	this.M = M;
	this.log = log;

	this.getContentPane().setLayout(new BorderLayout(0, 3));
	this.frameInit();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	makeWindow();
	this.setVisible(true);

    }

    protected void makeWindow() {
	panel = new JPanel();
	panel.setLayout(null);

	infoLabel = new JLabel("Was darf es sein?");

	einheitsMatrixButton = new MyButton(this);
	einheitsMatrixButton.setText("Einheitsmatrix");

	einheitsmatrix2Button = new MyButton(this);
	einheitsmatrix2Button.setText("Einheitsmatrix2");

	diagonalmatrixButton = new MyButton(this);
	diagonalmatrixButton.setText("Diagonalmatrix");

	hilbertmatrixButton = new MyButton(this);
	hilbertmatrixButton.setText("Hilbert-Matrix");
	
	begleitmatrixButton = new MyButton(this);
	begleitmatrixButton.setText("Begleitmatrix...");

	keineButton = new MyButton(this);
	keineButton.setText("Nichts");

	setPosition();
	addActionListeners();
	addComponents();
    }

    private void setPosition() {

	infoLabel.setBounds(40, 10, 250, 25);

	einheitsMatrixButton.setBounds   (10, 45,  200, 25);
	einheitsmatrix2Button.setBounds  (10, 85,  200, 25);
	diagonalmatrixButton.setBounds   (10, 125, 200, 25);
	hilbertmatrixButton.setBounds    (10, 175, 200, 25);
	begleitmatrixButton.setBounds    (10, 215, 200, 25);
	keineButton.setBounds            (10, 350, 200, 25);

    }

    private void addActionListeners() {
	einheitsMatrixButton.addActionListener(new ButtonActionListener(einheitsMatrixButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Einheitsmatrix erstellen!");

		sleepFix();

		((SpecialMatrixSelector)
			einheitsMatrixButton.getParentFrame())
		.M = new UnitMatrix(
			M.getId(),
				M.getLog(),
				M.getNumberOfRows(),
				M.getNumberOfCols(),
				M.getValues());
		
		String[][] newValues = M.toLaTeX2PNGStringArray();
		
		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Ausgangsmatrix",
			M.getNumberOfRows(),
			M.getNumberOfCols(),
			newValues,
			"Ausgangsmatrix",
			true);
				
		new RowOpSelector(M, log).setBounds(1300, 530, 230, 420);

	    }

	});

	einheitsmatrix2Button.addActionListener(new ButtonActionListener(einheitsmatrix2Button) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Einheitsmatrix mit 0 und 1 vertauscht erstellen!");

		sleepFix();
		((SpecialMatrixSelector)
			einheitsmatrix2Button.getParentFrame())
		.M = new UnitMatrix2(
			M.getId(),
				M.getLog(),
				M.getNumberOfRows(),
				M.getNumberOfCols(),
				M.getValues());
		
		String[][] newValues = M.toLaTeX2PNGStringArray();
		
		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Ausgangsmatrix",
			M.getNumberOfRows(),
			M.getNumberOfCols(),
			newValues,
			"Ausgangsmatrix",
			true);
				
		new RowOpSelector(M, log).setBounds(1300, 530, 230, 420);
	    }
	});

	diagonalmatrixButton.addActionListener(new ButtonActionListener(diagonalmatrixButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Diagonalmatrix erstellen!");

		sleepFix();
		new DiagonalMatrixInput(M, log);

	    }

	});

	hilbertmatrixButton
		.addActionListener(new ButtonActionListener(hilbertmatrixButton) {

		    @Override
		    public void actionPerformed(ActionEvent e) {

			System.out.println("Hilbert-Matrix erstellen!");			
			sleepFix();
			
			// new...
			
		    }

		});
	
	begleitmatrixButton
	.addActionListener(new ButtonActionListener(begleitmatrixButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Begleitmatrix erstellen!");			
		sleepFix();
		
		// new... BegleitmatrixInputHelper
		
	    }

	});

	keineButton.addActionListener(new ButtonActionListener(keineButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		System.out.println("keine weiteren!");
		keineButton.getParentFrame().dispose();
		sleepFix();
	    }
	});
    }

    private void addComponents() {

	panel.add(infoLabel);
	panel.add(einheitsMatrixButton);
	panel.add(einheitsmatrix2Button);
	panel.add(diagonalmatrixButton);
	panel.add(hilbertmatrixButton);
	panel.add(begleitmatrixButton);
//	panel.add(refButton);
//	panel.add(rrefButton);
	panel.add(keineButton);
	this.add(panel);

    }

}
