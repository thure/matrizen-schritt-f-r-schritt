package supertest;

public class DiagonalMatrix extends Matrix {

    public DiagonalMatrix(String id, Log log, int numberOfRows, int numberOfCols,
	    String[][] values, String[] diagonalEntries) {
	super(id, log, numberOfRows, numberOfCols, values);
	
	if (numberOfRows != numberOfCols) {
	    throw new IllegalArgumentException("Matrix must be square");
	}
	
	if (numberOfRows != diagonalEntries.length) {
	    throw new IllegalArgumentException("Wrong number of entries on diagonal");
	}
	
	for (int i = 0; i < numberOfRows; i++) {
	    for (int j = 0; j < numberOfCols; j++) {
		
		BigRational entry;
		
		if (i == j) {
		    entry = new BigRational(diagonalEntries[i]);
		} else {
		    entry = BigRational.ZERO;
		}
		this.values[i][j] = entry;
	    }	    
	}	
    }
    
    public DiagonalMatrix(String id, Log log, int numberOfRows, int numberOfCols,
	    BigRational[][] values, BigRational[] diagonalEntries) {
	super(id, log, numberOfRows, numberOfCols, values);
	
	if (numberOfRows != numberOfCols) {
	    throw new IllegalArgumentException("Matrix must be square");
	}
	
	if (numberOfRows != diagonalEntries.length) {
	    throw new IllegalArgumentException("Wrong number of entries on diagonal");
	}
	
	for (int i = 0; i < numberOfRows; i++) {
	    for (int j = 0; j < numberOfCols; j++) {
		this.values[i][j] = (i==j ? diagonalEntries[i] : BigRational.ZERO);
	    }	    
	}	
    }    
}
