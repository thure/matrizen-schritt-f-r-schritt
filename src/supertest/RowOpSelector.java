package supertest;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.GregorianCalendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import static supertest.Stuff.*;

public class RowOpSelector extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = -5712080261092922467L;

    private abstract class ButtonActionListener implements ActionListener {
	public JButton parent;

	public ButtonActionListener(JButton parentButton) {
	    this.parent = parentButton;
	}
    }

    JPanel panel;

    JLabel infoLabel;
    MyButton swapButton;
    MyButton scaleButton;
    MyButton addButton;
    MyButton noFractionsButton;
    MyButton gcdIsOneButton;
    MyButton refButton;
    MyButton rrefButton;    
    MyButton keineButton;

    Matrix M;
    Log log;

    public RowOpSelector(Matrix M, Log log) {

	this.M = M;
	this.log = log;

	this.getContentPane().setLayout(new BorderLayout(0, 3));
	this.frameInit();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	makeWindow();
	this.setVisible(true);

    }

    protected void makeWindow() {
	panel = new JPanel();
	panel.setLayout(null);

	infoLabel = new JLabel("Was darf es sein?");

	swapButton = new MyButton(this);
	swapButton.setText("Zeilen vertauschen");

	scaleButton = new MyButton(this);
	scaleButton.setText("Zeile skalieren");

	addButton = new MyButton(this);
	addButton.setText("Zeilen addieren");

	noFractionsButton = new MyButton(this);
	noFractionsButton.setText("Brüche beseitigen");
	
	gcdIsOneButton = new MyButton(this);
	gcdIsOneButton.setText("ggT herausziehen");
	
	refButton = new MyButton(this);
	refButton.setText("Zeilenstufenform");
	
	rrefButton = new MyButton(this);
	rrefButton.setText("reduzierte ZSF");

	keineButton = new MyButton(this);
	keineButton.setText("Nichts");

	setPosition();
	addActionListeners();
	addComponents();
    }

    private void setPosition() {

	infoLabel.setBounds(40, 10, 250, 25);

	swapButton.setBounds         (10, 45,  200, 25);
	scaleButton.setBounds        (10, 85,  200, 25);
	addButton.setBounds          (10, 125, 200, 25);
	noFractionsButton.setBounds  (10, 175, 200, 25);
	gcdIsOneButton.setBounds     (10, 215, 200, 25);
	refButton.setBounds          (10, 265, 200, 25);
	rrefButton.setBounds         (10, 305, 200, 25);
	keineButton.setBounds        (10, 350, 200, 25);

    }

    private void addActionListeners() {
	swapButton.addActionListener(new ButtonActionListener(swapButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Zeilen vertauschen!");

		sleepFix();
		new RowSwapper(M, log).setBounds(1250, 500, 340, 140);

	    }

	});

	scaleButton.addActionListener(new ButtonActionListener(scaleButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Zeilen skalieren!");

		sleepFix();
		new RowScaler(M, log).setBounds(1250, 500, 350, 140);
	    }
	});

	addButton.addActionListener(new ButtonActionListener(addButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Zeilen addieren!");

		sleepFix();
		new RowAdder(M, log).setBounds(1250, 500, 310, 140);

	    }

	});

	noFractionsButton
		.addActionListener(new ButtonActionListener(noFractionsButton) {

		    @Override
		    public void actionPerformed(ActionEvent e) {

			System.out.println("Brüche beseitigen!");			
			sleepFix();
			
			Matrix result = M.killAllFractions();
			
			result.setId(new Integer(Integer.parseInt(M.getId()) + 1)
				.toString());
			
			int rows = result.getNumberOfRows();
			int cols = result.getNumberOfCols();
			
			String[][] newValues = result.toLaTeX2PNGStringArray();
			
			GregorianCalendar c = new GregorianCalendar();
			
			
			// konstanter Dateiname // debug
			String id = "42";
			// String id = new Long(c.getTimeInMillis()).toString();
			
			
			//     System.err.println(M.toLaTeX2PNGStringArray());
			// nicht png, sondern pdf // debug
			new Latex2pdf().make(//"Umformungsschritt " + result.getId(),
				rows,
				cols, newValues, id, false);
			
		    }

		});
	
	gcdIsOneButton
	.addActionListener(new ButtonActionListener(gcdIsOneButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("ggT herausziehen!");			
		sleepFix();
		
		Matrix result = M.pullOutGCDinAllRows();
		
		result.setId(new Integer(Integer.parseInt(M.getId()) + 1)
			.toString());
		
		int rows = result.getNumberOfRows();
		int cols = result.getNumberOfCols();
		
		String[][] newValues = result.toLaTeX2PNGStringArray();
		
		GregorianCalendar c = new GregorianCalendar();
		
		// konstanter Dateiname // debug
		String id = "42";
		// String id = new Long(c.getTimeInMillis()).toString();
		
		//     System.err.println(M.toLaTeX2PNGStringArray());
		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Umformungsschritt " + result.getId(),
			rows,
			cols, newValues, id, false);
		
	    }

	});
	
	refButton
	.addActionListener(new ButtonActionListener(refButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Zeilenstufenform!");			
		sleepFix();
		
		Matrix result = M.toRowEchelonForm();
		
		result.setId(new Integer(Integer.parseInt(M.getId()) + 1)
			.toString());
		
		int rows = result.getNumberOfRows();
		int cols = result.getNumberOfCols();
		
		String[][] newValues = result.toLaTeX2PNGStringArray();
		
		GregorianCalendar c = new GregorianCalendar();
		
		// konstanter Dateiname // debug
		String id = "42";
		// String id = new Long(c.getTimeInMillis()).toString();
		
		//     System.err.println(M.toLaTeX2PNGStringArray());
		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Umformungsschritt " + result.getId(),
			rows,
			cols, newValues, id, false);
		
	    }

	});
	
	rrefButton
	.addActionListener(new ButtonActionListener(rrefButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("Reduzierte ZSF!");			
		sleepFix();
		
		Matrix result = M.toReducedRowEchelonForm();
		
		result.setId(new Integer(Integer.parseInt(M.getId()) + 1)
			.toString());
		
		int rows = result.getNumberOfRows();
		int cols = result.getNumberOfCols();
		
		String[][] newValues = result.toLaTeX2PNGStringArray();
		
		GregorianCalendar c = new GregorianCalendar();
		
		// konstanter Dateiname // debug
		String id = "42";
		// String id = new Long(c.getTimeInMillis()).toString();
		
		//     System.err.println(M.toLaTeX2PNGStringArray());
		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Umformungsschritt " + result.getId(),
			rows,
			cols, newValues, id, false);
		
	    }

	});

	keineButton.addActionListener(new ButtonActionListener(keineButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		System.out.println("keine weiteren!");
		keineButton.getParentFrame().dispose();
		sleepFix();
	    }
	});
    }

    private void addComponents() {

	panel.add(infoLabel);
	panel.add(swapButton);
	panel.add(scaleButton);
	panel.add(addButton);
	panel.add(noFractionsButton);
	panel.add(gcdIsOneButton);
//	panel.add(refButton);
//	panel.add(rrefButton);
	panel.add(keineButton);
	this.add(panel);

    }

}
