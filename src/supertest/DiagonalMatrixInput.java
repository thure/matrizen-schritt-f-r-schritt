package supertest;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import static supertest.Stuff.*;

public class DiagonalMatrixInput extends JFrame {

    /**
     * 
     */
    private static final long serialVersionUID = 5149948375274063620L;

    private abstract class ButtonActionListener implements ActionListener {
	public JButton parent;

	public ButtonActionListener(JButton parentButton) {
	    this.parent = parentButton;
	}
    }

    Log log;
    private int rows = -1;
    private int cols = -1;
    private String[][] values = null;
    private String[] diagonalEntries = null;
    private String NewLine = System.getProperty("line.separator");

    JPanel panel;
    JLabel diagonalInputLabel;

    JTextField diagonalInputField;

    MyButton OKButton;
    MyButton cancelButton;
    private Matrix M;

    public DiagonalMatrixInput(Matrix M, Log log) {

	this.M = M;
	this.log = log;

	this.getContentPane().setLayout(new BorderLayout(0, 3));
	this.frameInit();
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	makeWindow();
	this.setVisible(true);

    }

    protected void makeWindow() {
	System.out.println("Diagonalmatrix EINGEBEN");
	panel = new JPanel();
	panel.setLayout(null);
	diagonalInputLabel = new JLabel("Einträge in der Diagonalen");
	diagonalInputField = new JTextField(1);

	OKButton = new MyButton(this);
	OKButton.setText("OK");
	cancelButton = new MyButton(this);
	cancelButton.setText("Abbrechen");

	setPosition();
	addActionListeners();
	addComponents();
    }

    private void setPosition() {
	
	this.setBounds(1100, 530, 277, 170);

	diagonalInputLabel.setBounds(35, 5, 200, 25);
	diagonalInputField.setBounds(35, 35, 200, 25);

	OKButton.setBounds(37, 70, 200, 25);
	cancelButton.setBounds(37, 100, 200, 25);

    }

    private void addActionListeners() {
	OKButton.addActionListener(new ButtonActionListener(OKButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {

		System.out.println("OK from Helper!");

		String input = diagonalInputField.getText();
		diagonalEntries =  input.split(",");
		
		for (int i = 0; i < diagonalEntries.length; i++) {
		    String entry = diagonalEntries[i];		    
		    BigRational b = new BigRational(entry);
		    String z = b.tex();
		    diagonalEntries[i] = z;
		}

		OKButton.getParentFrame().dispose();
		sleepFix();

		rows = M.getNumberOfRows();
		cols = M.getNumberOfCols();
		values = new String[rows][cols];
		
		for (int i = 0; i < rows; i++) {
		    for (int j = 0; j < cols; j++) {
			values[i][j] = M.getValues()[i][j].toString();
		    }
		}
		
		System.out.println(Arrays.deepToString(diagonalEntries));		
		
		M = new DiagonalMatrix("0", log, rows, cols,
			values, diagonalEntries);
		
		System.out.println(Arrays.deepToString(M.getValues()));

		String[][] newValues = M.toLaTeX2PNGStringArray();

		log.setBounds(200, 530, 500, 320);
		log.getOrigMatrixArea().append(M.toInputString() + NewLine);

		// nicht png, sondern pdf // debug
		new Latex2pdf().make(//"Ausgangsmatrix",
			rows, cols, newValues,
			"Ausgangsmatrix", true);

		new RowOpSelector(M, log).setBounds(1300, 530, 230, 420);
	    }
	});

	cancelButton.addActionListener(new ButtonActionListener(cancelButton) {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		System.out.println("cancel");
		System.exit(1);
	    }
	});
    }

    private void addComponents() {

	panel.add(diagonalInputLabel);
	panel.add(diagonalInputField);
	panel.add(OKButton);
	panel.add(cancelButton);
	this.add(panel);

    }
}
