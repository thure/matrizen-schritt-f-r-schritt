/**
 * a class implementing exact arithmetic on rational numbers of arbitrary
 * magnitude
 */
package supertest;

/**
 * @author Robert Sedgewick and Kevin Wayne, Princeton University; tdu
 *
 */

import java.math.BigInteger;

public class BigRational implements Comparable<BigRational> {

    public final static BigRational ZERO = new BigRational(0);
    public final static BigRational ONE = new BigRational(1);
    

    private BigInteger num;   // the numerator
    private BigInteger den;   // the denominator


    // create and initialise a new BigRational object
    public BigRational(int numerator, int denominator) {
        this(new BigInteger("" + numerator), new BigInteger("" + denominator));
    }

    // create and initialise a new BigRational object that represents an integer
    public BigRational(int numerator) {
        this(numerator, 1);
    }

    // create and initialise a new BigRational object from a string,
    // e.g., "-343/1273"
    public BigRational(String s) {
        String[] tokens = s.split("/");
        if (tokens.length == 2)
            init(new BigInteger(tokens[0]), new BigInteger(tokens[1]));
        else if (tokens.length == 1)
            init(new BigInteger(tokens[0]), BigInteger.ONE);
        else
            throw new RuntimeException("Parse error in BigRational");
    }

    // create and initialise a new BigRational object from two BigIntegers
    public BigRational(BigInteger numerator, BigInteger denominator) {
        init(numerator, denominator);
    }

    private void init(BigInteger numerator, BigInteger denominator) {

        // deal with x / 0
        if (denominator.equals(BigInteger.ZERO)) {
           throw new RuntimeException("Denominator is zero");
        }

        // reduce fraction
        BigInteger g = numerator.gcd(denominator);
        num = numerator.divide(g);
        den = denominator.divide(g);

        // to ensure invariant that denominator is positive
        if (den.compareTo(BigInteger.ZERO) < 0) {
            den = den.negate();
            num = num.negate();
        }
    }

    public BigInteger getNumerator() {
        return num;
    }

    public BigInteger getDenominator() {
        return den;
    }

    // return string representation of (this)
    public String toString() { 
        if (isIntegral()) return num + "";
        else              return num + "/" + den;
    }
    
    public String tex() {
	// well, pseudo-tex really. I hate backslashes in java --
	// just look at the weird syntax for patterns!
	//
	// m = Pattern.compile("b\\{").matcher(m).replaceAll("\\\\frac\\{");
	//
	// Why does the brace ^
	// need to be escaped? Because it is special to regular expressions.
	// Why does the backslash that escapes the brace need to be
	// escaped another time? I have no idea...
	// and, at least for now, no intent to find out.
	if (isIntegral())
	    return num + "";
	else {
	    if (isNegative()) {
		return "-b{" + num.negate() + "}{" + den + "}";
	    } else {
		return "b{" + num + "}{" + den + "}";
	    }
	}
    }
    

    // return { -1, 0, + 1 } if a < b, a = b, or a > b
    public int compareTo(BigRational b) {
        BigRational a = this;
        return a.num.multiply(b.den).compareTo(a.den.multiply(b.num));
    }
    
    // integers can be treated specially in the tex code
    public boolean isIntegral() {
	return this.den.compareTo(BigInteger.ONE) == 0;
    }
    
    // some syntactic sugar for the above comparisons
    public boolean isGreaterThan(BigRational b) {
	return this.compareTo(b) > 0;
    }
    
    public boolean isLessThan(BigRational b) {
	return this.compareTo(b) < 0;
    }
    
    // is this BigRational negative, zero, or positive?
    public boolean isZero()     { return compareTo(ZERO) == 0; }
    public boolean isNonZero()  { return compareTo(ZERO) != 0; }
    public boolean isPositive() { return compareTo(ZERO)  > 0; }
    public boolean isNegative() { return compareTo(ZERO)  < 0; }

    // is this Rational object equal to y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;  
        if (y.getClass() != this.getClass()) return false;
        BigRational b = (BigRational) y;
        return compareTo(b) == 0;
    }
        
    // hashCode consistent with equals() and compareTo()
    public int hashCode() {
        return this.toString().hashCode();
    }
    

    // return a * b
    public BigRational times(BigRational b) {
        BigRational a = this;
        return new BigRational(a.num.multiply(b.num), a.den.multiply(b.den));
    }

    // return a + b
    public BigRational plus(BigRational b) {
        BigRational a = this;
        BigInteger numerator   = a.num.multiply(b.den).add(b.num.multiply(a.den));
        BigInteger denominator = a.den.multiply(b.den);
        return new BigRational(numerator, denominator);
    }   

    // return -a
    public BigRational negate() {
        return new BigRational(num.negate(), den);
    }

    // return a - b
    public BigRational minus(BigRational b) {
        BigRational a = this;
        return a.plus(b.negate());
    }

    // return 1 / a
    public BigRational reciprocal() {
        return new BigRational(den, num);
    }

    // return a / b
    public BigRational divideBy(BigRational b) {
        BigRational a = this;
        return a.times(b.reciprocal());
    }
}
